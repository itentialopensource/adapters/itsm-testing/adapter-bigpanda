/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-bigpanda',
      type: 'Bigpanda',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Bigpanda = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Bigpanda Adapter Test', () => {
  describe('Bigpanda Class Tests', () => {
    const a = new Bigpanda(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const alertsSendAlertBodyParam = {
      app_key: '<app_key>',
      status: 'critical',
      host: 'sanity-server-15',
      check: 'network overloaded',
      datacenter: 'sanity-dc-4',
      description: 'CPU is above upper limit (70%)',
      cluster: 'sanity-cluster-5',
      region: 'sanity-region-1'
    };
    describe('#sendAlert - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sendAlert(alertsSendAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'sendAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsAppKey = 'fakedata';
    const alertsSendAlertOIMBodyParam = {
      status: 'critical',
      host: 'sanity-server-15',
      check: 'network overloaded',
      datacenter: 'sanity-dc-4',
      description: 'CPU is above upper limit (70%)',
      cluster: 'sanity-cluster-5',
      region: 'sanity-region-1'
    };
    describe('#sendAlertOIM - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sendAlertOIM(alertsAppKey, alertsSendAlertOIMBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'sendAlertOIM', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersCreateUsersBodyParam = {
      userName: samProps.authentication.username,
      displayName: 'test_user',
      active: false,
      password: samProps.authentication.password
    };
    describe('#createUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createUsers(usersCreateUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'createUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAllUsers - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveAllUsers((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'retrieveAllUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUpdateaUserBodyParam = {
      userName: 'kitsen.andrey@gmail.com'
    };
    describe('#updateaUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateaUser(usersUpdateaUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'updateaUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenancePlansCreateaPlanBodyParam = {
      name: 'host1 maintenance',
      condition: {
        '=': [
          'host',
          'prod-api-1'
        ]
      },
      start: 1633904622,
      end: 1634768622
    };
    describe('#createaPlan - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createaPlan(maintenancePlansCreateaPlanBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenancePlans', 'createaPlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAllPlans - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveAllPlans((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenancePlans', 'retrieveAllPlans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulesV1CreateaScheduleBodyParam = {
      name: 'Weekend Maintenance',
      starts_on: 1491265491,
      ends_on: 1491294307,
      active: true
    };
    describe('#createaSchedule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createaSchedule(schedulesV1CreateaScheduleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SchedulesV1', 'createaSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAllSchedules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveAllSchedules((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SchedulesV1', 'retrieveAllSchedules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulesV1Id = 'fakedata';
    describe('#retrieveaSchedule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveaSchedule(schedulesV1Id, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SchedulesV1', 'retrieveaSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const enrichmentsId = 'fakedata';
    const enrichmentsUpdateAlertEnrichmentItemBodyParam = {
      when: {}
    };
    describe('#updateAlertEnrichmentItem - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAlertEnrichmentItem(enrichmentsId, enrichmentsUpdateAlertEnrichmentItemBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enrichments', 'updateAlertEnrichmentItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertEnrichmentTags - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAlertEnrichmentTags((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Enrichments', 'getAlertEnrichmentTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changesCreateUpdateaChangeBodyParam = {
      identifier: 'CNG-123',
      tags: {
        host: 'prod1.cluster1.app1',
        type: 'risky'
      },
      ticket_url: 'https://jira.link.com/ticket?change=chng123',
      summary: 'Summary of the change ',
      end: 1634583017,
      start: 1633891817,
      status: 'In Progress'
    };
    describe('#createUpdateaChange - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createUpdateaChange(changesCreateUpdateaChangeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Changes', 'createUpdateaChange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changesStartTimeFrame = 555;
    const changesEndTimeFrame = 555;
    describe('#retrieveAllChanges - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveAllChanges(changesStartTimeFrame, changesEndTimeFrame, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Changes', 'retrieveAllChanges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAllEnvironments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveAllEnvironments((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Environments', 'retrieveAllEnvironments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const environmentsId = 'fakedata';
    describe('#retrieveanEnvironmentbyID - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveanEnvironmentbyID(environmentsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Environments', 'retrieveanEnvironmentbyID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAllAuditLogs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveAllAuditLogs((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Audit', 'retrieveAllAuditLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveAllTroubleshootingLogs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveAllTroubleshootingLogs((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TroubleshootingLogs', 'retrieveAllTroubleshootingLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const correlationPatternsCreateCorrelationPatternBodyParam = {
      tags: [
        'service',
        'check'
      ],
      time_window: 60,
      active: true,
      cross_source: false
    };
    describe('#createCorrelationPattern - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createCorrelationPattern(correlationPatternsCreateCorrelationPatternBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CorrelationPatterns', 'createCorrelationPattern', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCorrelationPatterns - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCorrelationPatterns((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CorrelationPatterns', 'getCorrelationPatterns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertEnrichmentV2TagName = 'fakedata';
    describe('#listAllEnrichmentItemsofaTag - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.listAllEnrichmentItemsofaTag(alertEnrichmentV2TagName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlertEnrichmentV2', 'listAllEnrichmentItemsofaTag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentsV2Query = 'fakedata';
    const incidentsV2EnvironmentId = 'fakedata';
    describe('#searchIncidents - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.searchIncidents(incidentsV2Query, incidentsV2EnvironmentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IncidentsV2', 'searchIncidents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentsV2IncidentId = 'fakedata';
    describe('#retrieveIncidentbyID - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveIncidentbyID(incidentsV2EnvironmentId, incidentsV2IncidentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IncidentsV2', 'retrieveIncidentbyID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentTagsEnvId = 'fakedata';
    const incidentTagsIncidentId = 'fakedata';
    describe('#retrieveAllTagsforanIncident - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveAllTagsforanIncident(incidentTagsEnvId, incidentTagsIncidentId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IncidentTags', 'retrieveAllTagsforanIncident', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentTagsTagId = 'fakedata';
    describe('#retrieveanIncidentTagfromanIncident - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.retrieveanIncidentTagfromanIncident(incidentTagsEnvId, incidentTagsIncidentId, incidentTagsTagId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IncidentTags', 'retrieveanIncidentTagfromanIncident', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyUpdateTopologyBodyParam = {
      links: [
        {
          source: 'host',
          target: 'check'
        },
        {
          source: 'host',
          target: 'cluster'
        }
      ]
    };
    describe('#updateTopology - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateTopology(topologyUpdateTopologyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'updateTopology', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyCreateTopologyCopyBodyParam = {
      links: [
        {
          source: 'host',
          target: 'check'
        }
      ]
    };
    describe('#createTopologyCopy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTopologyCopy(topologyCreateTopologyCopyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'createTopologyCopy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTopologies - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAllTopologies((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'getAllTopologies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oIMConfigurationOIMConfigv1BodyParam = {
      config: {
        map_remaining: true,
        is_array: true,
        secondary_property: {
          source: [
            '@alert.labels.monitor',
            '@alert.labels.alertname'
          ]
        },
        array_source: 'alerts',
        primary_property: {
          source: [
            '@alert.labels.instance',
            '@alert.labels.job',
            '@alert.labels.alertname'
          ]
        },
        status: {
          default_to: 'critical',
          status_map: {
            warning: [
              'warn',
              'warning'
            ],
            critical: [
              'page',
              'critical'
            ],
            acknowledged: [
              'acknowledged'
            ],
            ok: [
              'resolved'
            ]
          },
          source: [
            '@alert.status',
            '@alert.labels.severity'
          ]
        },
        timestamp: {
          source: [
            '@alert.annotations.timestamp',
            '@alert.startsAt',
            '@alert.endsAt'
          ]
        },
        additional_attributes: [
          {
            source: [
              '@alert.startsAt'
            ]
          },
          {
            source: [
              '@alert.endsAt'
            ]
          },
          {
            source: [
              '@alert.generatorURL'
            ]
          },
          {
            flatten: true,
            source: [
              '@alert.annotations'
            ]
          },
          {
            flatten: false,
            source: [
              '@alert.annotations2'
            ]
          },
          {
            flatten: true,
            source: [
              '@alert.labels'
            ]
          },
          {
            source: [
              '@alert.fingerprint'
            ]
          }
        ]
      },
      samplePayload: {
        alerts: [
          {
            generatorURL: 'https://some-url.io',
            fingerprint: '123456789',
            annotations: {
              description: 'This alert is used to check the CPU utilization of this non-existent server',
              runbook_url: 'https://runbook.io/cpu-check'
            },
            startsAt: '2022-07-05T19:59:25.661Z',
            endsAt: '0001-01-01T00:00:00Z',
            status: 'firing',
            labels: {
              severity: 'page',
              instance: 'bigpanda-server',
              alertname: 'BigPanda Test',
              team: 'metrics',
              env: 'test',
              job: 'CPU Check',
              locale: 'ca',
              timestamp: '1.657051103137505e+09'
            }
          }
        ],
        commonLabels: {
          instance: 'bigpanda-server',
          alertname: 'BigPanda Test',
          team: 'metrics',
          env: 'test',
          job: 'CPU Check',
          locale: 'ca',
          timestamp: '1.657051103137505e+09'
        },
        externalURL: 'https://external-url.io',
        groupLabels: {
          instance: 'bigpanda-server',
          alertname: 'BigPanda Test',
          team: 'metrics',
          env: 'test',
          job: 'CPU Check',
          locale: 'ca',
          timestamp: '1.657051103137505e+09'
        },
        receiver: 'bigpanda',
        version: '4',
        commonAnnotations: {
          description: 'This alert is used to check the CPU utilization of this non-existent server',
          runbook_url: 'https://runbook.io/cpu-check'
        },
        status: 'firing'
      }
    };
    describe('#oIMConfigv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.oIMConfigv1(oIMConfigurationOIMConfigv1BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OIMConfiguration', 'oIMConfigv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const oIMConfigurationOIMConfigv2BodyParam = {
      config: {
        map_remaining: true,
        is_array: true,
        secondary_property: [
          {
            name: 'check'
          },
          {
            name: 'description'
          }
        ],
        array_source: 'alerts',
        primary_property: [
          {
            name: 'host'
          },
          {
            name: 'application'
          },
          {
            name: 'service'
          }
        ],
        version: '2.0',
        status: {
          default_to: 'critical',
          status_map: {
            warning: [
              'warn',
              'warning'
            ],
            critical: [
              'page',
              'critical'
            ],
            acknowledged: [
              'acknowledged'
            ],
            ok: [
              'resolved'
            ]
          },
          source: [
            '@alert.status',
            '@alert.labels.severity'
          ]
        },
        timestamp: {
          source: [
            '@alert.annotations.timestamp',
            '@alert.startsat',
            '@alert.endsat'
          ]
        },
        additional_attributes: [
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.alertname',
              '@alert.labels.check',
              '@alert.labels.alert',
              '@alert.labels.sdesc',
              '@alert.labels.short_desc',
              '@alert.labels.title'
            ],
            suggestedDestination: 'check',
            isDestinationAccepted: true,
            name: 'check',
            isSourceAccepted: true,
            source: [
              '@alert.labels.alertname',
              '@alert.labels.check',
              '@alert.labels.alert',
              '@alert.labels.sdesc',
              '@alert.labels.short_desc',
              '@alert.labels.title'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.app_name',
              '@alert.labels.application',
              '@alert.labels.app',
              '@alert.labels.impacted_app'
            ],
            suggestedDestination: 'application',
            isDestinationAccepted: true,
            name: 'application',
            isSourceAccepted: true,
            source: [
              '@alert.labels.app_name',
              '@alert.labels.application',
              '@alert.labels.app',
              '@alert.labels.impacted_app'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.assignment_group',
              '@alert.labels.support_group',
              '@alert.labels.routing_group',
              '@alert.labels.escalation_group',
              '@alert.labels.workgroup'
            ],
            suggestedDestination: 'assignment_group',
            isDestinationAccepted: true,
            isSourceAccepted: true,
            name: 'assignment_group',
            source: [
              '@alert.labels.assignment_group',
              '@alert.labels.support_group',
              '@alert.labels.routing_group',
              '@alert.labels.escalation_group',
              '@alert.labels.workgroup'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.summary',
              '@alert.labels.ldesc',
              '@alert.labels.long_desc',
              '@alert.labels.problem'
            ],
            suggestedDestination: 'description',
            isDestinationAccepted: true,
            name: 'description',
            isSourceAccepted: true,
            source: [
              '@alert.labels.summary',
              '@alert.labels.ldesc',
              '@alert.labels.long_desc',
              '@alert.labels.problem'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.host',
              '@alert.labels.hostname',
              '@alert.labels.instance',
              '@alert.labels.device',
              '@alert.labels.machine',
              '@alert.labels.server',
              '@alert.labels.hosts',
              '@alert.labels.node'
            ],
            suggestedDestination: 'host',
            isDestinationAccepted: true,
            name: 'host',
            isSourceAccepted: true,
            source: [
              '@alert.labels.host',
              '@alert.labels.hostname',
              '@alert.labels.instance',
              '@alert.labels.device',
              '@alert.labels.machine',
              '@alert.labels.server',
              '@alert.labels.hosts',
              '@alert.labels.node'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.location',
              '@alert.labels.physical_location',
              '@alert.labels.device_location'
            ],
            suggestedDestination: 'location',
            isDestinationAccepted: true,
            isSourceAccepted: true,
            name: 'location',
            source: [
              '@alert.labels.location',
              '@alert.labels.physical_location',
              '@alert.labels.device_location'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.priority',
              '@alert.labels.severity',
              '@alert.labels.inc_priority'
            ],
            suggestedDestination: 'priority',
            isDestinationAccepted: true,
            isSourceAccepted: true,
            name: 'priority',
            source: [
              '@alert.labels.priority',
              '@alert.labels.severity',
              '@alert.labels.inc_priority'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.service',
              '@alert.labels.impact_service',
              '@alert.labels.srvc'
            ],
            suggestedDestination: 'service',
            isDestinationAccepted: true,
            name: 'service',
            isSourceAccepted: true,
            source: [
              '@alert.labels.service',
              '@alert.labels.impact_service',
              '@alert.labels.srvc'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.impacted_ci',
              '@alert.labels.configuration_item',
              '@alert.labels.ci_name'
            ],
            suggestedDestination: 'cmdb_ci',
            isDestinationAccepted: true,
            isSourceAccepted: true,
            name: 'cmdb_ci',
            source: [
              '@alert.labels.impacted_ci',
              '@alert.labels.configuration_item',
              '@alert.labels.ci_name'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.business_unit',
              '@alert.labels.logical_group',
              '@alert.labels.lob'
            ],
            suggestedDestination: 'business_group',
            isDestinationAccepted: true,
            isSourceAccepted: true,
            name: 'business_group',
            source: [
              '@alert.labels.business_unit',
              '@alert.labels.logical_group',
              '@alert.labels.lob'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.inc_impact'
            ],
            suggestedDestination: 'impact',
            isDestinationAccepted: true,
            isSourceAccepted: true,
            name: 'impact',
            source: [
              '@alert.labels.inc_impact'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.inc_urgency'
            ],
            suggestedDestination: 'urgency',
            isDestinationAccepted: true,
            isSourceAccepted: true,
            name: 'urgency',
            source: [
              '@alert.labels.inc_urgency'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.env',
              '@alert.labels.tier',
              '@alert.labels.stage'
            ],
            suggestedDestination: 'environment',
            isDestinationAccepted: true,
            isSourceAccepted: true,
            name: 'environment',
            source: [
              '@alert.labels.env',
              '@alert.labels.tier',
              '@alert.labels.stage'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.router',
              '@alert.labels.routers',
              '@alert.labels.switch',
              '@alert.labels.switches',
              '@alert.labels.hub',
              '@alert.labels.repeater',
              '@alert.labels.bridge',
              '@alert.labels.gateway '
            ],
            suggestedDestination: 'network_device',
            isDestinationAccepted: true,
            isSourceAccepted: true,
            name: 'network_device',
            source: [
              '@alert.labels.router',
              '@alert.labels.routers',
              '@alert.labels.switch',
              '@alert.labels.switches',
              '@alert.labels.hub',
              '@alert.labels.repeater',
              '@alert.labels.bridge',
              '@alert.labels.gateway '
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.rack',
              '@alert.labels.tower'
            ],
            suggestedDestination: 'cluster',
            isDestinationAccepted: true,
            isSourceAccepted: true,
            name: 'cluster',
            source: [
              '@alert.labels.rack',
              '@alert.labels.tower'
            ]
          },
          {
            isSuggestion: true,
            suggestedSources: [
              '@alert.labels.runbook_link',
              '@alert.labels.knowledge_base',
              '@alert.labels.ki_article',
              '@alert.labels.support_link',
              '@alert.labels.kb_article',
              '@alert.labels.wiki_url'
            ],
            suggestedDestination: 'runbook_url',
            isDestinationAccepted: true,
            isSourceAccepted: true,
            name: 'runbook_url',
            source: [
              '@alert.labels.runbook_link',
              '@alert.labels.knowledge_base',
              '@alert.labels.ki_article',
              '@alert.labels.support_link',
              '@alert.labels.kb_article',
              '@alert.labels.wiki_url'
            ]
          },
          {
            isSuggestion: false,
            source: [
              '@alert.startsat'
            ]
          },
          {
            isSuggestion: false,
            source: [
              '@alert.endsat'
            ]
          },
          {
            isSuggestion: false,
            source: [
              '@alert.generatorurl'
            ]
          },
          {
            flatten: false,
            isSuggestion: false,
            source: [
              '@alert.annotations'
            ]
          },
          {
            flatten: true,
            isSuggestion: false,
            source: [
              '@alert.annotations2'
            ]
          },
          {
            flatten: true,
            isSuggestion: false,
            source: [
              '@alert.labels'
            ]
          },
          {
            isSuggestion: false,
            source: [
              '@alert.fingerprint'
            ]
          },
          {
            isSuggestion: false,
            source: [
              '@alert.labels.instance',
              '@alert.labels.job',
              '@alert.labels.alertname'
            ]
          },
          {
            isSuggestion: false,
            source: [
              '@alert.labels.monitor',
              '@alert.labels.alertname'
            ]
          }
        ]
      },
      sample_payload: {
        alerts: [
          {
            generatorURL: 'https://some-url.io',
            fingerprint: '123456789',
            annotations: {
              description: 'This alert is used to check the CPU utilization of this non-existent server',
              runbook_url: 'https://runbook.io/cpu-check'
            },
            startsAt: '2022-07-05T19:59:25.661Z',
            endsAt: '0001-01-01T00:00:00Z',
            status: 'firing',
            labels: {
              severity: 'page',
              instance: 'bigpanda-server',
              alertname: 'BigPanda Test',
              team: 'metrics',
              env: 'test',
              job: 'CPU Check',
              locale: 'ca',
              timestamp: '1.657051103137505e+09'
            }
          }
        ],
        commonLabels: {
          instance: 'bigpanda-server',
          alertname: 'BigPanda Test',
          team: 'metrics',
          env: 'test',
          job: 'CPU Check',
          locale: 'ca',
          timestamp: '1.657051103137505e+09'
        },
        externalURL: 'https://external-url.io',
        groupLabels: {
          instance: 'bigpanda-server',
          alertname: 'BigPanda Test',
          team: 'metrics',
          env: 'test',
          job: 'CPU Check',
          locale: 'ca',
          timestamp: '1.657051103137505e+09'
        },
        receiver: 'bigpanda',
        version: '4',
        commonAnnotations: {
          description: 'This alert is used to check the CPU utilization of this non-existent server',
          runbook_url: 'https://runbook.io/cpu-check'
        },
        status: 'firing'
      }
    };
    describe('#oIMConfigv2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.oIMConfigv2(oIMConfigurationOIMConfigv2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OIMConfiguration', 'oIMConfigv2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#oIMConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.oIMConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OIMConfiguration', 'oIMConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteaUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteaUser((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteaUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const topologyId = 'fakedata';
    describe('#deleteTopology - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTopology(topologyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-bigpanda-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Topology', 'deleteTopology', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
