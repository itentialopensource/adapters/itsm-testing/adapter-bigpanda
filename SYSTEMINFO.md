# BigPanda

Vendor: BigPanda
Homepage: https://www.bigpanda.io/

Product: BigPanda
Product Page: https://www.bigpanda.io/

## Introduction
We classify BigPanda into the ITSM (Service Management) domain as BigPanda provides capabilities to automate and streamline incident management and operational workflows.

"BigPanda enables you to maintain service reliability, speed incident resolution, maximize IT investments, and scale incident management" 

## Why Integrate
The BigPanda adapter from Itential is used to integrate the Itential Automation Platform (IAP) with BigPanda. With this adapter you have the ability to perform operations such as:

- Get Incidents
- Send Alert
- Create or Update Change

## Additional Product Documentation
The [API documents for BigPanda](https://docs.bigpanda.io/reference/introduction)