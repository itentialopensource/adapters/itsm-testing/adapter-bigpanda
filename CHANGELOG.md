
## 0.2.4 [10-14-2024]

* Changes made at 2024.10.14_18:37PM

See merge request itentialopensource/adapters/adapter-bigpanda!10

---

## 0.2.3 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-bigpanda!8

---

## 0.2.2 [08-14-2024]

* Changes made at 2024.08.14_17:17PM

See merge request itentialopensource/adapters/adapter-bigpanda!7

---

## 0.2.1 [08-07-2024]

* Changes made at 2024.08.06_18:17PM

See merge request itentialopensource/adapters/adapter-bigpanda!6

---

## 0.2.0 [07-23-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-bigpanda!5

---

## 0.1.5 [03-27-2024]

* Changes made at 2024.03.27_13:56PM

See merge request itentialopensource/adapters/itsm-testing/adapter-bigpanda!4

---

## 0.1.4 [03-13-2024]

* Changes made at 2024.03.13_15:00PM

See merge request itentialopensource/adapters/itsm-testing/adapter-bigpanda!3

---

## 0.1.3 [03-11-2024]

* Changes made at 2024.03.11_14:41PM

See merge request itentialopensource/adapters/itsm-testing/adapter-bigpanda!2

---

## 0.1.2 [02-28-2024]

* Changes made at 2024.02.28_11:04AM

See merge request itentialopensource/adapters/itsm-testing/adapter-bigpanda!1

---

## 0.1.1 [01-19-2024]

* Bug fixes and performance improvements

See commit feee7e9

---
